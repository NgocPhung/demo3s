﻿using System;
using Exercise01.Models;
using Exercise01.Repository.GenericRepository;

namespace Exercise01.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Employee> EmployeeRepository { get; }
        IGenericRepository<Request> RequestReponsitory { get; }
        void Save();

    }
}