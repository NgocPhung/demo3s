﻿using System;
using Exercise01.Models;
using Exercise01.Repository.GenericRepository;

namespace Exercise01.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ExerciseContext _context;

        public UnitOfWork(ExerciseContext context)
        {
            _context = context;
            InitReponsitory();
        }
        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IGenericRepository<Employee> EmployeeRepository { get; private set; }
        public IGenericRepository<Request> RequestReponsitory { get; private set; }
        
        private void InitReponsitory()
        {
            EmployeeRepository = new GenericRepository<Employee>(_context);
            RequestReponsitory = new GenericRepository<Request>(_context);
        }
        public void Save()
        {
            _context.SaveChanges();
        }

    }
}