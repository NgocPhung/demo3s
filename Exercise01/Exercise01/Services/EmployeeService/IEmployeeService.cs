﻿using System.Collections.Generic;
using Exercise01.Models;

namespace Exercise01.Services.EmployeeService
{
    public interface IEmployeeService
    {
        Employee GetById(int? id);
        IEnumerable<Employee> GetAll();
        void Add(Employee employee);
        void Delete(int id);
        void Update(Employee employee);
        void Save();
    }
}