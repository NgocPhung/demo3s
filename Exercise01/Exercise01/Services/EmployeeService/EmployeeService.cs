﻿using System.Collections.Generic;
using Exercise01.Models;
using Exercise01.UnitOfWork;

namespace Exercise01.Services.EmployeeService
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ExerciseContext _context;

        public EmployeeService(IUnitOfWork unitOfWork, ExerciseContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        public Employee GetById(int? id)
        {
            return _unitOfWork.EmployeeRepository.GetById(id);
        }

        public IEnumerable<Employee> GetAll()
        {
            return _unitOfWork.EmployeeRepository.GetAll();
        }

        public void Add(Employee employee)
        {
            _unitOfWork.EmployeeRepository.Add(employee);
        }

        public void Delete(int id)
        {
            _unitOfWork.EmployeeRepository.Delete(id);
        }

        public void Update(Employee employee)
        {
            _unitOfWork.EmployeeRepository.Update(employee);
        }

        public void Save()
        {
            _unitOfWork.EmployeeRepository.Save();
        }
        
    }
}