﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Exercise01.Models;
using Exercise01.UnitOfWork;

namespace Exercise01.Services.RequestService
{
    public class RequestService : IRequestService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ExerciseContext _context;

        public RequestService(IUnitOfWork unitOfWork, ExerciseContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        public Request GetById(int? id)
        {
            return _unitOfWork.RequestReponsitory.GetById(id);
        }

        public Task<PageList<Request>> GetsPageList(int pageSize, int pageNumber, string keyword)
        {
            throw new System.NotImplementedException();
        }

        public void Add(Request request)
        {
            _unitOfWork.RequestReponsitory.Add(request);
        }

        public void Delete(int id)
        {
            _unitOfWork.RequestReponsitory.Delete(id);
        }

        public void Update(Request request)
        {
            _unitOfWork.RequestReponsitory.Update(request);
        }

        public void Save()
        {
            _unitOfWork.RequestReponsitory.Save();
        }
    }
}