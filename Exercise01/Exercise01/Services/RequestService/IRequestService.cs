﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Exercise01.Models;

namespace Exercise01.Services.RequestService
{
    public interface IRequestService
    {
        Request GetById(int? id);
        void Add(Request request);
        void Delete(int id);
        void Update(Request request);
        void Save();
    }
}