﻿using System.Linq;
using System.Threading.Tasks;
using Exercise01.Models;
using Exercise01.Repository.GenericRepository;

namespace Exercise01.Repository.EmployeeRepository
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        private readonly ExerciseContext _context;
        public EmployeeRepository(ExerciseContext context) : base(context)
        {
            _context = context;
        }
        
        public async Task<PageList<Employee>> GetsPageList(int pageSize, int pageNumber, string keyword)
        {
            var queryable = _context.Employees.AsQueryable();
            
            if (keyword != null)
            {
                queryable = queryable.Where(e => e.Email.Contains(keyword)
                                                 || e.EmployeeName.Contains(keyword)
                                                 || e.Address.Contains(keyword));

            }
            
            var response = await new PageList<Employee>().GetItemPageList(queryable, pageSize, pageNumber, keyword); 

            return response;
        }
    }
}