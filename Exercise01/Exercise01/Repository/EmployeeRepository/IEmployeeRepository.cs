﻿using System.Threading.Tasks;
using Exercise01.Models;
using Exercise01.Repository.GenericRepository;

namespace Exercise01.Repository.EmployeeRepository
{
    public interface IEmployeeRepository : IGenericRepository<Employee>
    {
        Task<PageList<Employee>> GetsPageList(int pageSize, int pageNumber, string keyword);
    }
}