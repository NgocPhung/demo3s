﻿using System.Threading.Tasks;
using Exercise01.Models;
using Exercise01.Repository.GenericRepository;

namespace Exercise01.Repository.RequestRepository
{
    public interface IRequestRepository : IGenericRepository<Request>
    {
        Task<PageList<Request>> GetsPageList(int pageSize, int pageNumber, string keyword);
    }
}