﻿using System.Linq;
using System.Threading.Tasks;
using Exercise01.Models;
using Exercise01.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace Exercise01.Repository.RequestRepository
{
    public class RequestRepository : GenericRepository<Request>, IRequestRepository
    {
        private ExerciseContext _context;
        public RequestRepository(ExerciseContext context) : base(context)
        {
            _context = context;
        }

        public async Task<PageList<Request>> GetsPageList(int pageSize, int pageNumber, string keyword)
        {
            //_context.Requests: Lay tat ca du lieu bang Request
            // .Include("Employee"): Lay them du lieu bang Employee vi Request co quan he truong du lieu voi bang Employee
            
            var queryable = _context.Requests.Include("Employee").AsQueryable();
            if (keyword != null)
            {
                queryable = queryable.Where(r => r.ApproveBy.Contains(keyword) 
                                                 || r.Subject.Contains(keyword) 
                                                 || r.Content.Contains(keyword));
            }

            var reponse = await new PageList<Request>().GetItemPageList(queryable, pageSize, pageNumber, keyword);
            return reponse;
        }
    }
}