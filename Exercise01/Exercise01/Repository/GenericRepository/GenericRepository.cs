﻿using System.Collections.Generic;
using System.Linq;
using Exercise01.Models;
using Microsoft.EntityFrameworkCore;

namespace Exercise01.Repository.GenericRepository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private ExerciseContext _context;
        private DbSet<TEntity> _dbSet;

        public GenericRepository(ExerciseContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }
        public TEntity GetById(int? id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity entity = _dbSet.Find(id);
            _dbSet.Remove(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
            _context.SaveChanges();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}