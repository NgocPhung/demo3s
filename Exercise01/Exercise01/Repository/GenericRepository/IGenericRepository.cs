﻿using System.Collections.Generic;

namespace Exercise01.Repository.GenericRepository
{
    public interface IGenericRepository<TEntity>
    {
        TEntity GetById(int? id);
        IEnumerable<TEntity> GetAll();
        void Add(TEntity entity);
        void Delete(int id);
        void Update(TEntity entity);
        void Save();

    }
}