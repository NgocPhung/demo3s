﻿using System.Linq;
using System.Threading.Tasks;

namespace Exercise01.Models
{
    public interface IPageList<T>
    {
        int PageSize { get; set; }
        int PageNumber { get; set; }
        long TotalRecord { get; set; }
        string Name { get; set; }
        

        Task<PageList<T>> GetItemPageList(IQueryable<T> queryable, int pageSize, int pageNumber, string keyword);
    }
}