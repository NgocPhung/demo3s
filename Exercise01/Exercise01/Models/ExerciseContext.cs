﻿using Microsoft.EntityFrameworkCore;

namespace Exercise01.Models
{
    public class ExerciseContext : DbContext
    {
        public ExerciseContext (DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Request> Requests { get; set; }
    }
}