﻿using System;
using System.Collections.Generic;

namespace Exercise01.Models
{
    public class Request
    {
        public int RequestId { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string ApproveBy { get; set; }
        private List<Employee> Employees { get; }
        public Request()
        {
            Employees = new List<Employee>();
        }

        
        public int EmployeeId { get; set; }
        
        //The hien quan he voi bang Employee
        //Request la
        public Employee Employee { get; set; }
    }
}