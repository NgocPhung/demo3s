﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Exercise01.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        
        [StringLength(50, ErrorMessage = "Name length can't be more than 50")]
        public string EmployeeName { get; set; }
        
        [StringLength(200, ErrorMessage = "Name length can't be more than 200")]
        public string Address { get; set; }
        
        [StringLength(50)]
        [DataType(DataType.EmailAddress, ErrorMessage = "")]
        public string Email { get; set; }
        
        //The hien quan he voi bang Requests
        // Employee la phia 1
        public ICollection<Request> Requests { get; set; }
    }
}