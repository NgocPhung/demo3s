﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Exercise01.Models
{
    public class PageList<T> : IPageList<T>
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public long TotalRecord { get; set; }
        public string Name { get; set; }


        private long TotalPage { get; set; }
        public List<T> Data { get; private set; }

        public async Task<PageList<T>> GetItemPageList(IQueryable<T> queryable, int pageSize, int pageNumber,
            string keyword)
        {
            var totalRecord = await queryable.LongCountAsync();
            List<T> itemsOnPage;
            if (pageSize == -1) // truowng hop nay neu truyen len pageSize = -1 thi tra ve tat ca
                itemsOnPage = await queryable.ToListAsync();
            else
                itemsOnPage = await queryable
                    .Skip(pageSize * pageNumber)
                    .Take(pageSize)
                    .ToListAsync();
            var totalPage = totalRecord % pageSize > 0 ? totalRecord / pageSize + 1 : totalRecord / pageSize;

            return new PageList<T>
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                TotalRecord = totalRecord,
                Data = itemsOnPage,
                TotalPage = totalPage
            };
        }
    }
}