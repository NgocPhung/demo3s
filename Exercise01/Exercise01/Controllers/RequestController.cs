﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Exercise01.Models;
using Exercise01.Repository.RequestRepository;
using Exercise01.Services.EmployeeService;
using Exercise01.Services.RequestService;
using Microsoft.AspNetCore.Mvc;

namespace Exercise01.Controllers
{
    public class RequestController : Controller
    {
        private readonly IRequestService _requestService;
        private readonly IRequestRepository _requestRepository;
        private readonly IEmployeeService _employeeService;

        public RequestController(IRequestService requestService, IRequestRepository requestRepository, IEmployeeService employeeService)
        {
            _requestService = requestService;
            _requestRepository = requestRepository;
            _employeeService = employeeService;
        }
        [HttpGet]
        // GET
        public async Task<IActionResult> Index(int pageSize=10, int pageNumber=0, string keyword=null)
        {
            var response = await _requestRepository.GetsPageList(pageSize, pageNumber, keyword);
            return View(response);
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            var employees = _employeeService.GetAll();
            ViewBag.Employees = employees;
            return View();
        }

        [HttpPost]
        public IActionResult Create(Request request)
        {
            _requestService.Add(request);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var employees = _employeeService.GetAll();
            ViewBag.Employees = employees;
            var request = _requestService.GetById(id);
            
            return View(request);
        }

        [HttpPost]
        public IActionResult Edit(Request request)
        {
            _requestService.Update(request);
            return RedirectToAction("Index");
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _requestService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}