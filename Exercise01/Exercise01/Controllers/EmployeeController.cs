﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Exercise01.Models;
using Exercise01.Repository.EmployeeRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace Exercise01.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private ExerciseContext _context;

        public EmployeeController(IEmployeeRepository employeeRepository, ExerciseContext context)
        {
            _employeeRepository = employeeRepository;
            _context = context;
        }
        [HttpGet]
        // GET
        public  async Task<IActionResult> Index( int pageSize=10, int pageNumber=0, string keyword=null)
        {
            var response = await _employeeRepository.GetsPageList(pageSize, pageNumber, keyword);
            return View(response);
        }
        
        
        
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Employee employee)
        {
            _employeeRepository.Add(employee);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var getId = _employeeRepository.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Edit(Employee employee)
        {
            _employeeRepository.Update(employee);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _employeeRepository.Delete(id);
            return RedirectToAction("Index");
        }
        
    }
}